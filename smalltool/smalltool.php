<?php
/*
Plugin Name: 小工具
Plugin URI: https://www.beizigen.com/post/wordpress-on-off-common-function-widget/
Description: 快速开启、关闭一些功能。
Version: 2.0
Author: 背字根
Author URI: https://www.beizigen.com/
Text Domain: smalltool
License: GPL v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.txt
*/

include plugin_dir_path(__FILE__) . 'tools.php';
new smallTools;

//添加菜单
add_action('admin_menu', function () {
	add_options_page('小工具', '小工具', 'administrator', 'smalltool', 'smalltool_control');
});

//插件页面设置链接
add_filter('plugin_action_links', function ($links, $file) {
	if ($file == plugin_basename(__FILE__)) {
		array_unshift($links, '<a href="' . admin_url('options-general.php?page=smalltool') . '">设置</a>');
	}
	return $links;
}, 10, 2);

function smalltool_control()
{
	$options = array(
		['var_name' => 'open_sans', 'label_name' => '禁用 Google Open Sans 字体'],
		['var_name' => 'emoji', 'label_name' => '禁用 emoji 表情'],
		['var_name' => 'link_manage', 'label_name' => '开启友情链接管理'],
		['var_name' => 'admin_bar', 'label_name' => '关闭前台顶部工具条'],
		['var_name' => 'pingback', 'label_name' => '禁止向站内链接发送 PingBack 引用通告'],
		['var_name' => 'xml_rpc', 'label_name' => '移除 XML-RPC'],
		['var_name' => 'xml_rpc_api', 'label_name' => '禁用 XML-RPC 接口'],
		['var_name' => 'windows_live_writer', 'label_name' => '移除 Windows Live Writer'],
		['var_name' => 'wp_version', 'label_name' => '移除前台head标签中显示WP版本号'],
		['var_name' => 'css_js_version', 'label_name' => '移除CSS/JS版本号'],
		['var_name' => 'wp_shortlink', 'label_name' => '移除短链接'],
		['var_name' => 'login_errors', 'label_name' => '移除后台登录错误提示'],
		['var_name' => 'dashboard_activity', 'label_name' => '删除仪表盘“活动”模块'],
		['var_name' => 'dashboard_primary', 'label_name' => '删除仪表盘“WordPress 新闻”模块'],
		['var_name' => 'welcome_panel', 'label_name' => '删除仪表盘欢迎模块'],
		['var_name' => 'editor', 'label_name' => '使用经典编辑器'],
		['var_name' => 'post_trackbacksdiv', 'label_name' => '删除文章编辑页面 trackback 模块'],
		['var_name' => 'post_postcustom', 'label_name' => '删除文章编辑页面自定义字段模块'],
		['var_name' => 'capital_P_dangit', 'label_name' => '移除WP自动修正大小写'],
		['var_name' => 'wp_revisions_to_keep', 'label_name' => '禁用WP文章修订版'],
		['var_name' => 'no_lang', 'label_name' => '禁止前台加载语言包'],
		['var_name' => 'no_cron', 'label_name' => '禁用 WP Cron'],
		['var_name' => 'auto_embeds', 'label_name' => '禁用 auto-embeds'],
		['var_name' => 'post_embed', 'label_name' => '禁用 Post Embed'],
		['var_name' => 'file_name', 'label_name' => '重命名上传文件'],
		['var_name' => 'search_filter_page', 'label_name' => '不搜索页面'],
		['var_name' => 'comment_mail_notify', 'label_name' => '评论回复邮件通知'],
		['var_name' => 'no_medium_large', 'label_name' => '禁止缩略图尺寸medium_large'],
		['var_name' => 'remove_recentcomments_style', 'label_name' => '移除.recentcomments样式'],
		['var_name' => 'remove_block_library_style', 'label_name' => '移除Block Library样式'],
		['var_name' => 'global_styles_inline_css', 'label_name' => '移除Global样式'],
		['var_name' => 'add_webp', 'label_name' => '增加WebP支持'],
		['var_name' => 'passwd_blowfish', 'label_name' => '密码增强，使用Blowfish算法'],
		['var_name' => 'gravatar_cdn', 'label_name' => 'Gravatar头像使用国内源'],
		['var_name' => 'cdn_replace', 'label_name' => '静态资源CDN加速，启用后需要<a href="' . admin_url('options-general.php') . '#cdn-domain">设置CDN域名</a>。'],
	);

	if (isset($_POST['action']) && 'save' == $_POST['action']) {
		if (!empty($_POST['options'])) {
			$keys = array_column($options, 'var_name');
			$values = array_values($_POST['options']);
			$intersect = array_intersect($keys, $values);
			if (count($intersect) == count($values)) {
				update_option('smalltool', json_encode($_POST['options']));
			}
			echo '<div class="notice is-dismissible updated">设置保存成功！</div>';
		} else {
			delete_option('smalltool');
		}
	}

?>
	<div class="mywrap">
		<style type="text/css">
			#wpwrap .notice {
				margin-left: 0;
				margin-top: 15px;
				padding: 1em;
				border-radius: 4px;
			}

			.mywrap {
				background-color: #fff;
				padding: 5px 20px;
				border-radius: 4px;
				border: 1px solid #ccc;
				margin: 15px 15px 15px 0;
			}
		</style>

		<form method="post" action="<?php echo admin_url('options-general.php?page=smalltool'); ?>">
			<?php
			$option_value = get_option('smalltool');
			$option_value = $option_value ? json_decode($option_value, true) : array();
			foreach ($options as $option) {
				$checked = in_array($option['var_name'], $option_value) ? ' checked' : '';
				echo '<p><label><input type="checkbox" name="options[]" value="' . $option['var_name'] . '"' . $checked . '> ' . $option['label_name'] . '</label></p>';
			}
			?>
			<p class="submit">
				<input type="submit" class="button-primary" value="保存设置">
				<input type="hidden" name="action" value="save">
			</p>
		</form>
	</div>
<?php } ?>