# WordPress小工具

## 功能说明

* 禁用 Google Open Sans 字体；
* 禁用 emoji 表情；
* 开启友情链接管理；
* 关闭前台顶部工具条；
* 禁止向站内链接发送 PingBack 引用通告；
* 移除 XML-RPC；
* 禁用 XML-RPC 接口；
* 移除 Windows Live Writer；
* 移除前台head标签中显示WP版本号；
* 移除短链接；
* 移除后台登录错误提示；
* 删除仪表盘“活动”模块；
* 删除仪表盘“WordPress 新闻”模块；
* 删除仪表盘欢迎模块；
* 使用经典编辑器；
* 删除文章编辑页面 trackback 模块；
* 删除文章编辑页面自定义字段模块；
* 移除WP自动修正大小写；
* 禁用WP文章自动保存；
* 禁用WP文章修订版；
* 禁止前台加载语言包；
* 禁用 WP Cron；
* 禁用 auto-embeds；
* 禁用 Post Embed；
* 重命名上传文件；
* 不搜索页面；
* 评论回复邮件通知；
* 禁止缩略图尺寸medium_large；
* 移除.recentcomments样式；
* 取消内容转义；
* 取消摘要转义；
* 取消评论转义；
* 增加WebP支持；
* 使用v2ex的Gravatar镜像；
* 密码增强，使用Blowfish算法。
